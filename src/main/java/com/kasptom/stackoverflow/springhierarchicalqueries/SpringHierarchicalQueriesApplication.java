package com.kasptom.stackoverflow.springhierarchicalqueries;

import com.kasptom.stackoverflow.springhierarchicalqueries.services.PersonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHierarchicalQueriesApplication {


	public static void main(String[] args) {
		var ctx = SpringApplication.run(SpringHierarchicalQueriesApplication.class, args);

		PersonService personService = (PersonService) ctx.getBean("personService");

		System.out.println(personService.search("Boris", "5600"));
	}

}
