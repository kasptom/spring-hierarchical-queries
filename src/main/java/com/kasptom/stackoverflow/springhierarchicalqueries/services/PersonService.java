package com.kasptom.stackoverflow.springhierarchicalqueries.services;

import com.kasptom.stackoverflow.springhierarchicalqueries.domain.Person;
import com.kasptom.stackoverflow.springhierarchicalqueries.domain.QBoardMember;
import com.kasptom.stackoverflow.springhierarchicalqueries.domain.QEmployee;
import com.kasptom.stackoverflow.springhierarchicalqueries.domain.QPerson;
import com.kasptom.stackoverflow.springhierarchicalqueries.repositories.PersonRepository;
import com.querydsl.core.BooleanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    public List<Person> search(String firstName, String salary) {
        var searchCriterias = new BooleanBuilder();
        if (firstName != null) {
            searchCriterias.and(QPerson.person.firstName.eq(firstName));
        }
        if (salary != null) {
            searchCriterias.andAnyOf(
                    QPerson.person.as(QEmployee.class).salary.eq(salary),
                    QPerson.person.as(QBoardMember.class).salary.eq(salary)
            );
        }

        var result = new ArrayList<Person>();
        for (Person person : personRepository.findAll(searchCriterias)) {
            result.add(person);
        }
        return result;
    }
}
