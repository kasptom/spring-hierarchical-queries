package com.kasptom.stackoverflow.springhierarchicalqueries.bootsrtrapper;

import com.kasptom.stackoverflow.springhierarchicalqueries.domain.BoardMember;
import com.kasptom.stackoverflow.springhierarchicalqueries.domain.Employee;
import com.kasptom.stackoverflow.springhierarchicalqueries.domain.Person;
import com.kasptom.stackoverflow.springhierarchicalqueries.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Bootstrapper implements CommandLineRunner {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public void run(String... args) throws Exception {
        var person = new Person("Andrew", "McAndrew");
        var person2 = new Person("Boris", "Kalashnikov");

        var employee = new Employee("Andrew", "McAndrew", "5600");
        var boardMember = new BoardMember("Boris", "Kalashnikov", "5600");

        personRepository.save(person);
        personRepository.save(person2);
        personRepository.save(employee);
        personRepository.save(boardMember);
    }
}
