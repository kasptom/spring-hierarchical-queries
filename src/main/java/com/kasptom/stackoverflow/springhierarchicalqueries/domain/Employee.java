package com.kasptom.stackoverflow.springhierarchicalqueries.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Employee extends Person {

    @Column
    private String salary;

    public Employee(String firstName, String lastName, String salary) {
        super(firstName, lastName);
        this.salary = salary;
    }

    public Employee() {
    }
}
